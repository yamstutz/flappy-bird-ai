import pygame
import os
import random

PIPE_IMG =  pygame.transform.scale2x(pygame.image.load(os.path.join("imgs", "pipe.png")))

class Pipe:
    GAP = 200 # Gap between pipes
    VEL = 5   # How fast the pipes move

    def __init__(self, x):
        self.x = x          # X pipe cordinates
        self.height = 0     # Overall start of the pipe, could be outside the screen

        self.top = 0        # Top pipe cordinates
        self.bottom = 0     # Bottom pipe cordinates
        self.PIPE_TOP = pygame.transform.flip(PIPE_IMG, False, True) # Top pipe image
        self.PIPE_BOTTOM = PIPE_IMG                                  # Bottom pipe image

        self.passed = False # If the bird passed the pipe
        self.set_height()

    # Sets a height for the pipe
    def set_height(self):
        self.height = random.randrange(50, 450)
        self.top = self.height - self.PIPE_TOP.get_height() # To get absolute cordinates of the pipe
        self.bottom = self.height + self.GAP

    def move(self):
        self.x -= self.VEL

    def draw(self, win):
        win.blit(self.PIPE_TOP, (self.x, self.top))
        win.blit(self.PIPE_BOTTOM, (self.x, self.bottom))

    # Used for detecting pixel perfect collision
    def collide(self, bird):
        # Mask is an 2d array of all the cordinates of an object pixels
        bird_mask = bird.get_mask()
        top_mask = pygame.mask.from_surface(self.PIPE_TOP)
        bottom_mask = pygame.mask.from_surface(self.PIPE_BOTTOM)

        top_offset = (self.x - bird.x, self.top - round(bird.y))
        bottom_offset = (self.x - bird.x, self.bottom - round(bird.y))

        # The point of collision between the bird mask and the bottom pipe, if not collide returns None
        b_point = bird_mask.overlap(bottom_mask, bottom_offset)
        t_point = bird_mask.overlap(bottom_mask, top_offset)

        if t_point or b_point:
            return True

        return False   