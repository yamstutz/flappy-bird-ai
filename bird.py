import pygame
import os

BIRD_IMGS = [pygame.transform.scale2x(pygame.image.load(os.path.join("imgs", "bird1.png"))),
            pygame.transform.scale2x(pygame.image.load(os.path.join("imgs", "bird2.png"))),
            pygame.transform.scale2x(pygame.image.load(os.path.join("imgs", "bird3.png")))]

class Bird:
    IMGS = BIRD_IMGS    # List of bird images 0=Down, 1=Neutral, 2=Up
    MAX_ROTATION = 25   # Tilt for nose
    ROT_VEL = 20        # Rotation degree 
    ANIMATION_TIME = 5  # Each bird animation time

    def __init__(self, x, y):
        self.x = x              # Y cordinate in pygame
        self.y = y              # X cordinate in pygame
        self.tilt = 0           # Image tilt in degress
        self.tick_count = 0     # The sequence of frames we moved in a row iwthout jumping up
        self.vel = 0            # Velocity of the bird
        self.height = self.y    # Bird height
        self.img_count = 0      # How many images are displayed
        self.img = self.IMGS[0] # Neutral bird image state

    def jump(self):
        # (0,0) pixle is top left on pygame, so going up is negetive velocity
        self.vel = -10.5    
        self.tick_count = 0
        self.height = self.y

    def move(self):
        self.tick_count += 1

        # Physics formula for equal acceleration motion and gets how much we moved up or down
        # this.tick = time
        # this.vel = acceleration
        y_moved = self.vel*self.tick_count + 1.5*self.tick_count**2 

        # so we won't move down too fast
        if y_moved >= 16:
            y_moved = 16
        # before we move up move a little more down 
        if y_moved < 0:
            y_moved -= 2    

        self.y = self.y + y_moved  

        # Moving upwards
        if y_moved < 0 or self.y < self.height + 50:
            if self.tilt < self.MAX_ROTATION:
                # immediately tilt the bird
                self.tilt = self.MAX_ROTATION
        # Moving downwards        
        else:
            # so we don't nosedive to the ground
            if self.tilt > -90:
                self.tilt -= self.ROT_VEL        

    def draw(self, win):
        self.img_count += 1            

        # So it looks like the bird is flying
        if self.img_count < self.ANIMATION_TIME:
            self.img = self.IMGS[0]
        elif self.img_count < self.ANIMATION_TIME*2:
            self.img = self.IMGS[1]    
        elif self.img_count < self.ANIMATION_TIME*3:
            self.img = self.IMGS[2]    
        elif self.img_count < self.ANIMATION_TIME*4:
            self.img = self.IMGS[1]    
        elif self.img_count == self.ANIMATION_TIME*4 + 1:
            self.img = self.IMGS[0]
            self.img_count = 0

        # So it doesn't look like it's skips a frame
        if self.tilt <= -80:
            self.img = self.IMGS[1]    
            self.img_count = self.ANIMATION_TIME

         # So it's centers the rotated image and print it on screen
        rotated_image = pygame.transform.rotate(self.img, self.tilt)
        new_rect = rotated_image.get_rect(center=self.img.get_rect(topleft = (self.x, self.y)).center)
        win.blit(rotated_image, new_rect.topleft)

    # Called when we get a collsion
    def get_mask(self):    
        return pygame.mask.from_surface(self.img)