import pygame
import neat
import time
import os
import random
from bird import Bird
from pipe import Pipe
from base import Base
pygame.font.init()

GEN = 0
WIN_WIDTH = 560
WIN_HEIGHT = 700

# doubles the size of the image and then load them
BG_IMG =  pygame.transform.scale2x(pygame.image.load(os.path.join("imgs", "bg.png")))
STAT_FONT = pygame.font.SysFont("comicsans", 50)


def draw_window(win, birds, pipes, base, score):
    win.blit(BG_IMG, (0,-300)) # (0,0) is top left position 
    
    for pipe in pipes:
        pipe.draw(win)

    text = STAT_FONT.render("Score: " + str(score), 1, (255,255,255))
    win.blit(text, (WIN_HEIGHT - 150 - text.get_width(), 10))

    text = STAT_FONT.render("Gen: " + str(GEN), 1, (255,255,255))
    win.blit(text, (10, 10))

    base.draw(win)

    for bird in birds:
        bird.draw(win)

    pygame.display.update()


def main(genomes, config):
    global GEN
    GEN += 1
    nats = []
    ge = []
    birds = []

    # Setting up a neural network for this genome
    for _, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nats.append(net)
        birds.append(Bird(230, 350))
        g.fitness = 0
        ge.append(g)


    base = Base(600)
    pipes = [Pipe(600)]
    fps = 30
    score = 0 
    win = pygame.display.set_mode((WIN_WIDTH,WIN_HEIGHT))
    clock = pygame.time.Clock()

    run = True
    while run:
        #clock.tick(fps) # To run at 30 frames per second
        for event in pygame.event.get():
            # Red x on top left ,ake it quit
            if event.type == pygame.QUIT:
                run = False
                pygame.quit() # exits pygame
                quit()        # exits the program

        pipe_index = 0
        if len(birds) > 0:
            # If we passed the first pipe on the screen than look for the second pipe
            if len(pipes) > 1 and birds[0].x > pipes[0].x + pipes[0].PIPE_TOP.get_width():
                pipe_index = 1
                
        # If there's no birds quit the game
        else:
            run = False
            break

        for x, bird in enumerate(birds):
            bird.move()
            ge[x].fitness += 0.1

            # Checks if the bird is above or below a pipe
            output = nats[x].activate((bird.y, abs(bird.y - pipes[pipe_index].height), abs(bird.y - pipes[pipe_index].bottom)))

            if output[0] > 0.5:
                bird.jump()


        add_pipe = False
        rem = []
        for pipe in pipes:
            for x, bird in enumerate(birds):
                # Every time a bird hits a pipe remove one fitness score
                if pipe.collide(bird):
                    ge[x].fitness -= 1
                    birds.pop(x) # So we stop running the bird
                    nats.pop(x)  # Remove bird from neural network
                    ge.pop(x)    # So we stop tracking it's fitness

                # Checks if we passed the pipe
                if not pipe.passed and pipe.x < bird.x:
                    pipe.passed =True
                    add_pipe = True

            # Pipe is off the screen so add another one
            if pipe.x + pipe.PIPE_TOP.get_width() < 0:
                rem.append(pipe)

            pipe.move() 

        # if we passed pipe generate another one    
        if add_pipe:
            score += 1
            for g in ge:
                g.fitness += 5 # Adds 5 fitness points to the birds that made it through the pipe
            pipes.append(Pipe(600))

        # Removes the last pipe from our pipe list
        for r in rem:
            pipes.remove(r)

        # Checks if bird hit the ground or is above the pipe outside of the screen
        for x, bird in enumerate(birds):    
            if bird.y + bird.img.get_height() >= 600 or bird.y < 0:
                birds.pop(x)
                nats.pop(x)
                ge.pop(x)

        base.move()
        draw_window(win, birds, pipes, base, score)  


def run(config_path):
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                                neat.DefaultSpeciesSet, neat.DefaultStagnation,
                                config_path)

    p = neat.Population(config) # Generates a population                                

    # Print stats
    p.add_reporter(neat.StdOutReporter(True)) 
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
        
    winner = p.run(main, 50) # Runs 50 generations of the fitness fucntion    

if __name__ == "__main__":
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config.txt")
    run(config_path)